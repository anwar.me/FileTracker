<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\File;
use Illuminate\Http\Request;

Route::get('/', function () {
    $file = null;
    return view('welcome', compact('file'));
});

Route::post('/', function (Request $request) {
    $request->validate([
        'file_number' => 'required'
    ]);

    $file = File::where('file_number', $request->file_number)->get()->first();

    return view('welcome', compact('file'));

})->name('track');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/update/{job}', 'HomeController@update')->name('update');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
