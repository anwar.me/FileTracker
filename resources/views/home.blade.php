@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">All Pending jobs for you</div>

                    <div class="card-body">

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table table-bordered table-striped">

                            @if(Auth::user()->jobs->count())
                                <thead>
                                <th>#</th>
                                <th>File Number</th>
                                <th>Assigned At</th>
                                <th>Actions</th>
                                </thead>
                            @endif


                            <tbody>
                            @forelse(Auth::user()->jobs as $job)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $job->file->file_number }}</td>
                                    <td>{{ $job->created_at }}</td>
                                    <td>
                                        <a href="{{ route('update', $job) }}">
                                            <button class="btn btn-success btn-sm">Done</button>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>No more Jobs for you. Do some fun</tr>
                            @endforelse
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
