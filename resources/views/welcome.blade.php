<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>File Tracker</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="top-right links">

        <a href="">Track File</a>
        <a href="">Generate Application</a>

        @if (Route::has('login'))
            @auth
                <a href="{{ url('/home') }}">Dashboard</a>
            @else
                <a href="{{ route('login') }}">Login</a>
            @endauth

        @endif

    </div>


    <div class="content">
        <div class="title m-b-md">
            File Tracker
        </div>

        <div>
            <form action="{{ route('track') }}" method="post">

                @csrf

                <label for="file_number">File Number :</label>
                <input type="text" name="file_number" id="file_number" placeholder="e.g 1001"/>
                <button type="submit">Track</button>
            </form>

        </div>

        <div>
            @if($file)
                File Status
                <ul>
                    @foreach($file->jobs as $job)
                        <li>{{ $job->branch->branch_name }} - {{ ucfirst($job->status) }}</li>
                    @endforeach
                </ul>
            @endif
        </div>

        <div class="links">

        </div>
    </div>
</div>
</body>
</html>
