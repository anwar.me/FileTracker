<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public function file()
    {
        return $this->belongsTo(File::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
}
