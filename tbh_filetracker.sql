-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `branches`;
CREATE TABLE `branches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `branch_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `branches_branch_name_unique` (`branch_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `branches` (`id`, `branch_name`, `description`, `created_at`, `updated_at`, `user_id`) VALUES
(1,	'Zindabazer',	'Zindabazer branch',	'2018-08-12 10:53:38',	'2018-08-12 22:05:45',	2),
(2,	'Bondor Bazer',	'Bondor Bazer Branch',	'2018-08-12 10:53:59',	'2018-08-12 10:53:59',	0),
(3,	'Amborkhan',	'Amborkhan branch',	'2018-08-12 10:54:35',	'2018-08-12 10:54:35',	0);

DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1,	1,	'id',	'number',	'ID',	1,	0,	0,	0,	0,	0,	NULL,	1),
(2,	1,	'name',	'text',	'Name',	1,	1,	1,	1,	1,	1,	NULL,	2),
(3,	1,	'email',	'text',	'Email',	1,	1,	1,	1,	1,	1,	NULL,	3),
(4,	1,	'password',	'password',	'Password',	1,	0,	0,	1,	1,	0,	NULL,	4),
(5,	1,	'remember_token',	'text',	'Remember Token',	0,	0,	0,	0,	0,	0,	NULL,	5),
(6,	1,	'created_at',	'timestamp',	'Created At',	0,	1,	1,	0,	0,	0,	NULL,	6),
(7,	1,	'updated_at',	'timestamp',	'Updated At',	0,	0,	0,	0,	0,	0,	NULL,	7),
(8,	1,	'avatar',	'image',	'Avatar',	0,	1,	1,	1,	1,	1,	NULL,	8),
(9,	1,	'user_belongsto_role_relationship',	'relationship',	'Role',	0,	1,	1,	1,	1,	0,	'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}',	10),
(10,	1,	'user_belongstomany_role_relationship',	'relationship',	'Roles',	0,	1,	1,	1,	1,	0,	'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',	11),
(12,	1,	'settings',	'hidden',	'Settings',	0,	0,	0,	0,	0,	0,	NULL,	12),
(13,	2,	'id',	'number',	'ID',	1,	0,	0,	0,	0,	0,	'',	1),
(14,	2,	'name',	'text',	'Name',	1,	1,	1,	1,	1,	1,	'',	2),
(15,	2,	'created_at',	'timestamp',	'Created At',	0,	0,	0,	0,	0,	0,	'',	3),
(16,	2,	'updated_at',	'timestamp',	'Updated At',	0,	0,	0,	0,	0,	0,	'',	4),
(17,	3,	'id',	'number',	'ID',	1,	0,	0,	0,	0,	0,	'',	1),
(18,	3,	'name',	'text',	'Name',	1,	1,	1,	1,	1,	1,	'',	2),
(19,	3,	'created_at',	'timestamp',	'Created At',	0,	0,	0,	0,	0,	0,	'',	3),
(20,	3,	'updated_at',	'timestamp',	'Updated At',	0,	0,	0,	0,	0,	0,	'',	4),
(21,	3,	'display_name',	'text',	'Display Name',	1,	1,	1,	1,	1,	1,	'',	5),
(22,	1,	'role_id',	'text',	'Role',	0,	1,	1,	1,	1,	1,	NULL,	9),
(23,	4,	'id',	'text',	'Id',	1,	0,	0,	0,	0,	0,	NULL,	1),
(24,	4,	'file_number',	'text',	'File Number',	1,	1,	1,	1,	1,	1,	NULL,	2),
(25,	4,	'file_description',	'text_area',	'File Description',	0,	1,	1,	1,	1,	1,	NULL,	3),
(26,	4,	'created_at',	'timestamp',	'Created At',	0,	1,	1,	1,	0,	1,	NULL,	4),
(27,	4,	'updated_at',	'timestamp',	'Updated At',	0,	1,	0,	0,	0,	0,	NULL,	5),
(28,	5,	'id',	'text',	'Id',	1,	0,	0,	0,	0,	0,	NULL,	1),
(29,	5,	'branch_name',	'text',	'Branch Name',	1,	1,	1,	1,	1,	1,	NULL,	2),
(30,	5,	'description',	'text_area',	'Description',	0,	1,	1,	1,	1,	1,	NULL,	3),
(31,	5,	'created_at',	'timestamp',	'Created At',	0,	0,	1,	0,	0,	0,	NULL,	4),
(32,	5,	'updated_at',	'timestamp',	'Updated At',	0,	0,	1,	0,	0,	0,	NULL,	5),
(33,	6,	'id',	'text',	'Id',	1,	0,	0,	0,	0,	0,	NULL,	1),
(34,	6,	'file_id',	'text',	'File Number',	1,	1,	1,	1,	1,	1,	NULL,	2),
(36,	6,	'status',	'text',	'Status',	0,	1,	1,	1,	1,	1,	NULL,	6),
(37,	6,	'created_at',	'timestamp',	'Created At',	0,	1,	1,	1,	0,	1,	NULL,	7),
(38,	6,	'updated_at',	'timestamp',	'Updated At',	0,	1,	1,	0,	0,	0,	NULL,	8),
(39,	6,	'job_belongsto_branch_relationship',	'relationship',	'branches',	0,	1,	1,	1,	1,	1,	'{\"model\":\"App\\\\Branch\",\"table\":\"branches\",\"type\":\"belongsTo\",\"column\":\"branch_id\",\"key\":\"id\",\"label\":\"branch_name\",\"pivot_table\":\"branches\",\"pivot\":\"0\",\"taggable\":\"0\"}',	5),
(40,	6,	'job_belongsto_file_relationship',	'relationship',	'files',	0,	1,	1,	1,	1,	1,	'{\"model\":\"App\\\\File\",\"table\":\"files\",\"type\":\"belongsTo\",\"column\":\"file_id\",\"key\":\"id\",\"label\":\"file_number\",\"pivot_table\":\"branches\",\"pivot\":\"0\",\"taggable\":\"0\"}',	4),
(41,	6,	'branch_id',	'text',	'Branch Id',	1,	1,	1,	1,	1,	1,	NULL,	3),
(42,	5,	'branch_belongsto_user_relationship',	'relationship',	'users',	0,	1,	1,	1,	1,	1,	'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"branches\",\"pivot\":\"0\",\"taggable\":\"0\"}',	6),
(43,	5,	'user_id',	'text',	'User Id',	1,	1,	1,	1,	1,	1,	NULL,	6),
(44,	1,	'user_hasone_branch_relationship',	'relationship',	'branches',	0,	1,	1,	1,	1,	1,	'{\"model\":\"App\\\\Branch\",\"table\":\"branches\",\"type\":\"hasOne\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"branch_name\",\"pivot_table\":\"branches\",\"pivot\":\"0\",\"taggable\":\"0\"}',	13);

DROP TABLE IF EXISTS `data_types`;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1,	'users',	'users',	'User',	'Users',	'voyager-person',	'TCG\\Voyager\\Models\\User',	'TCG\\Voyager\\Policies\\UserPolicy',	NULL,	NULL,	1,	0,	'{\"order_column\":null,\"order_display_column\":null}',	'2018-08-12 10:11:49',	'2018-08-12 22:03:41'),
(2,	'menus',	'menus',	'Menu',	'Menus',	'voyager-list',	'TCG\\Voyager\\Models\\Menu',	NULL,	'',	'',	1,	0,	NULL,	'2018-08-12 10:11:50',	'2018-08-12 10:11:50'),
(3,	'roles',	'roles',	'Role',	'Roles',	'voyager-lock',	'TCG\\Voyager\\Models\\Role',	NULL,	'',	'',	1,	0,	NULL,	'2018-08-12 10:11:50',	'2018-08-12 10:11:50'),
(4,	'files',	'files',	'File',	'Files',	NULL,	'App\\File',	NULL,	NULL,	NULL,	1,	0,	'{\"order_column\":null,\"order_display_column\":null}',	'2018-08-12 10:49:46',	'2018-08-12 10:49:46'),
(5,	'branches',	'branches',	'Branch',	'Branches',	NULL,	'App\\Branch',	NULL,	NULL,	NULL,	1,	0,	'{\"order_column\":null,\"order_display_column\":null}',	'2018-08-12 10:52:56',	'2018-08-12 10:52:56'),
(6,	'jobs',	'jobs',	'Job',	'Jobs',	NULL,	'App\\Job',	NULL,	NULL,	NULL,	1,	0,	'{\"order_column\":null,\"order_display_column\":null}',	'2018-08-12 21:54:09',	'2018-08-12 21:54:09');

DROP TABLE IF EXISTS `files`;
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_number` int(11) NOT NULL,
  `file_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `files_file_number_unique` (`file_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `files` (`id`, `file_number`, `file_description`, `created_at`, `updated_at`) VALUES
(1,	1001,	'Abdul Kasem\'s Return file',	'2018-08-12 10:56:22',	'2018-08-12 10:56:22');

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `jobs` (`id`, `file_id`, `branch_id`, `status`, `created_at`, `updated_at`) VALUES
(1,	1,	1,	'done',	'2018-08-12 22:06:00',	'2018-08-12 22:58:15');

DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1,	'admin',	'2018-08-12 10:11:51',	'2018-08-12 10:11:51');

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1,	1,	'Dashboard',	'',	'_self',	'voyager-boat',	NULL,	NULL,	1,	'2018-08-12 10:11:51',	'2018-08-12 10:11:51',	'voyager.dashboard',	NULL),
(2,	1,	'Media',	'',	'_self',	'voyager-images',	NULL,	NULL,	5,	'2018-08-12 10:11:51',	'2018-08-12 10:11:51',	'voyager.media.index',	NULL),
(3,	1,	'Users',	'',	'_self',	'voyager-person',	NULL,	NULL,	3,	'2018-08-12 10:11:51',	'2018-08-12 10:11:51',	'voyager.users.index',	NULL),
(4,	1,	'Roles',	'',	'_self',	'voyager-lock',	NULL,	NULL,	2,	'2018-08-12 10:11:51',	'2018-08-12 10:11:51',	'voyager.roles.index',	NULL),
(5,	1,	'Tools',	'',	'_self',	'voyager-tools',	NULL,	NULL,	9,	'2018-08-12 10:11:51',	'2018-08-12 10:11:51',	NULL,	NULL),
(6,	1,	'Menu Builder',	'',	'_self',	'voyager-list',	NULL,	5,	10,	'2018-08-12 10:11:51',	'2018-08-12 10:11:51',	'voyager.menus.index',	NULL),
(7,	1,	'Database',	'',	'_self',	'voyager-data',	NULL,	5,	11,	'2018-08-12 10:11:51',	'2018-08-12 10:11:51',	'voyager.database.index',	NULL),
(8,	1,	'Compass',	'',	'_self',	'voyager-compass',	NULL,	5,	12,	'2018-08-12 10:11:51',	'2018-08-12 10:11:51',	'voyager.compass.index',	NULL),
(9,	1,	'BREAD',	'',	'_self',	'voyager-bread',	NULL,	5,	13,	'2018-08-12 10:11:52',	'2018-08-12 10:11:52',	'voyager.bread.index',	NULL),
(10,	1,	'Settings',	'',	'_self',	'voyager-settings',	NULL,	NULL,	14,	'2018-08-12 10:11:52',	'2018-08-12 10:11:52',	'voyager.settings.index',	NULL),
(11,	1,	'Hooks',	'',	'_self',	'voyager-hook',	NULL,	5,	13,	'2018-08-12 10:11:55',	'2018-08-12 10:11:55',	'voyager.hooks',	NULL),
(12,	1,	'Files',	'',	'_self',	NULL,	NULL,	NULL,	15,	'2018-08-12 10:49:47',	'2018-08-12 10:49:47',	'voyager.files.index',	NULL),
(13,	1,	'Branches',	'',	'_self',	NULL,	NULL,	NULL,	16,	'2018-08-12 10:52:57',	'2018-08-12 10:52:57',	'voyager.branches.index',	NULL),
(14,	1,	'Jobs',	'',	'_self',	NULL,	NULL,	NULL,	17,	'2018-08-12 21:54:09',	'2018-08-12 21:54:09',	'voyager.jobs.index',	NULL);

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2016_01_01_000000_add_voyager_user_fields',	1),
(4,	'2016_01_01_000000_create_data_types_table',	1),
(5,	'2016_05_19_173453_create_menu_table',	1),
(6,	'2016_10_21_190000_create_roles_table',	1),
(7,	'2016_10_21_190000_create_settings_table',	1),
(8,	'2016_11_30_135954_create_permission_table',	1),
(9,	'2016_11_30_141208_create_permission_role_table',	1),
(10,	'2016_12_26_201236_data_types__add__server_side',	1),
(11,	'2017_01_13_000000_add_route_to_menu_items_table',	1),
(12,	'2017_01_14_005015_create_translations_table',	1),
(13,	'2017_01_15_000000_make_table_name_nullable_in_permissions_table',	1),
(14,	'2017_03_06_000000_add_controller_to_data_types_table',	1),
(15,	'2017_04_21_000000_add_order_to_data_rows_table',	1),
(16,	'2017_07_05_210000_add_policyname_to_data_types_table',	1),
(17,	'2017_08_05_000000_add_group_to_settings_table',	1),
(18,	'2017_11_26_013050_add_user_role_relationship',	1),
(19,	'2017_11_26_015000_create_user_roles_table',	1),
(20,	'2018_03_11_000000_add_user_settings',	1),
(21,	'2018_03_14_000000_add_details_to_data_types_table',	1),
(22,	'2018_03_16_000000_make_settings_value_nullable',	1);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1,	'browse_admin',	NULL,	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(2,	'browse_bread',	NULL,	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(3,	'browse_database',	NULL,	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(4,	'browse_media',	NULL,	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(5,	'browse_compass',	NULL,	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(6,	'browse_menus',	'menus',	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(7,	'read_menus',	'menus',	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(8,	'edit_menus',	'menus',	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(9,	'add_menus',	'menus',	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(10,	'delete_menus',	'menus',	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(11,	'browse_roles',	'roles',	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(12,	'read_roles',	'roles',	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(13,	'edit_roles',	'roles',	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(14,	'add_roles',	'roles',	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(15,	'delete_roles',	'roles',	'2018-08-12 10:11:53',	'2018-08-12 10:11:53'),
(16,	'browse_users',	'users',	'2018-08-12 10:11:53',	'2018-08-12 10:11:53'),
(17,	'read_users',	'users',	'2018-08-12 10:11:53',	'2018-08-12 10:11:53'),
(18,	'edit_users',	'users',	'2018-08-12 10:11:53',	'2018-08-12 10:11:53'),
(19,	'add_users',	'users',	'2018-08-12 10:11:53',	'2018-08-12 10:11:53'),
(20,	'delete_users',	'users',	'2018-08-12 10:11:53',	'2018-08-12 10:11:53'),
(21,	'browse_settings',	'settings',	'2018-08-12 10:11:53',	'2018-08-12 10:11:53'),
(22,	'read_settings',	'settings',	'2018-08-12 10:11:53',	'2018-08-12 10:11:53'),
(23,	'edit_settings',	'settings',	'2018-08-12 10:11:53',	'2018-08-12 10:11:53'),
(24,	'add_settings',	'settings',	'2018-08-12 10:11:53',	'2018-08-12 10:11:53'),
(25,	'delete_settings',	'settings',	'2018-08-12 10:11:53',	'2018-08-12 10:11:53'),
(26,	'browse_hooks',	NULL,	'2018-08-12 10:11:55',	'2018-08-12 10:11:55'),
(27,	'browse_files',	'files',	'2018-08-12 10:49:47',	'2018-08-12 10:49:47'),
(28,	'read_files',	'files',	'2018-08-12 10:49:47',	'2018-08-12 10:49:47'),
(29,	'edit_files',	'files',	'2018-08-12 10:49:47',	'2018-08-12 10:49:47'),
(30,	'add_files',	'files',	'2018-08-12 10:49:47',	'2018-08-12 10:49:47'),
(31,	'delete_files',	'files',	'2018-08-12 10:49:47',	'2018-08-12 10:49:47'),
(32,	'browse_branches',	'branches',	'2018-08-12 10:52:57',	'2018-08-12 10:52:57'),
(33,	'read_branches',	'branches',	'2018-08-12 10:52:57',	'2018-08-12 10:52:57'),
(34,	'edit_branches',	'branches',	'2018-08-12 10:52:57',	'2018-08-12 10:52:57'),
(35,	'add_branches',	'branches',	'2018-08-12 10:52:57',	'2018-08-12 10:52:57'),
(36,	'delete_branches',	'branches',	'2018-08-12 10:52:57',	'2018-08-12 10:52:57'),
(37,	'browse_jobs',	'jobs',	'2018-08-12 21:54:09',	'2018-08-12 21:54:09'),
(38,	'read_jobs',	'jobs',	'2018-08-12 21:54:09',	'2018-08-12 21:54:09'),
(39,	'edit_jobs',	'jobs',	'2018-08-12 21:54:09',	'2018-08-12 21:54:09'),
(40,	'add_jobs',	'jobs',	'2018-08-12 21:54:09',	'2018-08-12 21:54:09'),
(41,	'delete_jobs',	'jobs',	'2018-08-12 21:54:09',	'2018-08-12 21:54:09');

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1,	1),
(1,	3),
(2,	1),
(3,	1),
(4,	1),
(5,	1),
(6,	1),
(7,	1),
(8,	1),
(9,	1),
(10,	1),
(11,	1),
(12,	1),
(13,	1),
(14,	1),
(15,	1),
(16,	1),
(17,	1),
(18,	1),
(19,	1),
(20,	1),
(21,	1),
(22,	1),
(23,	1),
(24,	1),
(25,	1),
(26,	1),
(27,	1),
(28,	1),
(29,	1),
(30,	1),
(31,	1),
(32,	1),
(33,	1),
(34,	1),
(35,	1),
(36,	1),
(37,	1),
(38,	1),
(39,	1),
(40,	1),
(41,	1);

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1,	'admin',	'Administrator',	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(2,	'user',	'Normal User',	'2018-08-12 10:11:52',	'2018-08-12 10:11:52'),
(3,	'employee',	'Employee',	'2018-08-12 10:44:12',	'2018-08-12 10:44:12');

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1,	'site.title',	'Site Title',	'Site Title',	'',	'text',	1,	'Site'),
(2,	'site.description',	'Site Description',	'Site Description',	'',	'text',	2,	'Site'),
(3,	'site.logo',	'Site Logo',	'',	'',	'image',	3,	'Site'),
(4,	'site.google_analytics_tracking_id',	'Google Analytics Tracking ID',	NULL,	'',	'text',	4,	'Site'),
(5,	'admin.bg_image',	'Admin Background Image',	'',	'',	'image',	5,	'Admin'),
(6,	'admin.title',	'Admin Title',	'File Tracker',	'',	'text',	1,	'Admin'),
(7,	'admin.description',	'Admin Description',	'Welcome to Voyager. The Missing Admin for Laravel',	'',	'text',	2,	'Admin'),
(8,	'admin.loader',	'Admin Loader',	'',	'',	'image',	3,	'Admin'),
(9,	'admin.icon_image',	'Admin Icon Image',	'',	'',	'image',	4,	'Admin'),
(10,	'admin.google_analytics_client_id',	'Google Analytics Client ID (used for admin dashboard)',	NULL,	'',	'text',	1,	'Admin');

DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1,	1,	'Administrator',	'admin@admin.com',	'users/August2018/KhPU2Oz8LE1HimeSyBxX.jpg',	'$2y$10$7X8pHfilzMo7h7kbpoyGw.knWAGU570tXN8BpBRVdl0wfypkGy4le',	NULL,	'{\"locale\":\"en\"}',	'2018-08-12 10:37:51',	'2018-08-12 10:42:15'),
(2,	3,	'Anwar Hussen',	'anwar.hussen3@gmail.com',	'users/August2018/mdfKWO6BXlbw1bpD3RrL.jpg',	'$2y$10$mL3B1HsHcLqV65hr2byamuvjyK0/GcoEqsm9LInD7to7SJ8Gelw0S',	NULL,	NULL,	'2018-08-12 22:05:03',	'2018-08-12 22:05:03');

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2018-08-13 17:03:40
